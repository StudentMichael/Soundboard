import React from 'react';

class Toolbar extends React.Component {
    // constructor(props) {
    //     super(props);
    //     this.filterValue = props.filterValue;
    //     this.filterChange = props.filterChange;
    // }

    // componentDidUpdate(prevProps, prevState, snapshot) {
    //     console.log('props ' + prevProps.filterValue + ' -> ' + this.props.filterValue);
    // }

    render() {
        //console.log('render ' + this.filterValue + ' - ' + this.props.filterValue);
        const {filterValue, filterChange} = this.props;
        return(<div className = "filters">
            <select>
            <option value = "1">1</option>
            </select>
            <input 
            type="text" 
            name = "textFilter" 
            label = "Soundtracks"
            onChange = {({target}) => filterChange(target.value.toUpperCase())} 
            value = {filterValue}
             />

      </div>)
    }
}

export default Toolbar;