import React from 'react';
import './App.css';
import Toolbar from './components/Toolbar';
import Item from './components/Item';
import data from './Data/data';
import Soundboard from './components/Soundboard';

class App extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      filterValue:'',
      items:data[0].items,
      audio:null
    }
  }

  //filterChange (e) {
    //Lambda doet automatische bind, maakt this mogelijk, onthoud scope
  filterChange = (e) => {
    this.setState({filterValue:e});
    if(e.value === '') {
      this.setState({items:data[0].items});
    }
    else {
      this.setState({items:data[0].items.filter(e => e.name.toUpperCase().includes(this.filterValue))});
    }
  }

  playSound = (e) =>  {
    this.setState({audio:new Audio(e.sound).play()});
  }

  render () {
    return(
    <div className="App">
      <h1>React Soundboard</h1>
      <Toolbar filterValue= {this.state.filterValue} filterChange = {this.filterChange/*.bind(this)*/}/>
      
      <Soundboard>
        {this.state.items.map(
        item => <Item item = {item} playSound = {this.playSound}/>            
        )} 
      </Soundboard>
    </div>
  );
  }
}

export default App;