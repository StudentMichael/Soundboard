import nierSound from "../assets/sounds/nier.mp3";
import marioSound from "../assets/sounds/mario.mp3";
import zeldaSound from "../assets/sounds/zelda.mp3";
import haloSound from "../assets/sounds/halo.mp3";
import metalGearSound from "../assets/sounds/metalGear.mp3";
import hotlineMiamiSound from "../assets/sounds/hotlineMiami.mp3";
import donkeyKongSound from "../assets/sounds/donkeyKong.mp3";
//https://imgur.com/a/yqN88N2
const data = [{
    name: "Soundtracks",
    items: [{
        name: "Nier: Gestalt",
        img: "https://i.imgur.com/Ko0QCZO.jpg",
        sound: nierSound
    },
    {
        name: "Mario",
        img: "https://i.imgur.com/t9apDQ4.jpg",
        sound: marioSound
    },
    {
        name: "Legend of Zelda",
        img: "https://i.imgur.com/fY6ZqRv.jpg",
        sound: zeldaSound
    },
    {
        name: "Halo",
        img: "https://i.imgur.com/g6AZthz.jpg",
        sound: haloSound
    },
    {
        name: "Metal Gear",
        img: "https://i.imgur.com/gQD0FzO.jpg",
        sound: metalGearSound
    },
    {
        name: "Hotline Miami",
        img: "https://i.imgur.com/V2DSc1k.jpg",
        sound: hotlineMiamiSound
    },
    {
        name: "Donkey Kong Country",
        img: "https://i.imgur.com/ghucpBF.jpg",
        sound: donkeyKongSound
    }]
}]

export default data;